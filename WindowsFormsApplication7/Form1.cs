﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace WindowsFormsApplication7
{
    public partial class Form1 : Form
    {
        public int paso = 0;
        public string valor1;
        public Button Boton;
        public Button Boton2;
        public ImageList ListaImagenes;
        public int Aciertos = 0;
        public int Intentos = 0;
        const int FinAciertos = 10;
        
        //CARGAMOS LOS SONIDOS
        SoundPlayer AbrirSound = new SoundPlayer(Properties.Resources.Abrir);
        SoundPlayer FallarSound = new SoundPlayer(Properties.Resources.Fallar);
        SoundPlayer AcertarSound = new SoundPlayer(Properties.Resources.Acertar);
              
        // ARRAY PARA GUARDAR EL INDEX DE LA FOTO QUE ESCOGERE,
        // PONGO UNO MAS YA QUE EL INDEX0 NO LO VOY A USAR PARA FACILITAR EL ENTENDIMIENTO DEL CODIGO
        int[] n = new int[21] { 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10 };

        //array para guardar el aleatorio que luego usare para que los index de las fotos sean aleatorios
        int[] Rnd = new int[21]; 
        public Form1()
        {
            InitializeComponent();


        }
        private void Form1_Load(object sender, EventArgs e)
        {
            // pasamos el imagelist a listaimagenes, que será el que se pase en las funciones
            ListaImagenes = ImgList;
            //Cargamos la funcion de cargar cada imagen en un cuadro
            Cargar_Imagenes();
            
        }
        //CADA VEZ QUE PULSAMOS UN BOTON, SE LLAMA A LA FUNCIONA COMPROBAR_ACCION
        private void button1_Click(object sender, EventArgs e)
        {
            Comprobar_Accion(this.button1, 1, this.pictureBox1, this.ListaImagenes);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Comprobar_Accion(this.button2, 2, this.pictureBox2, this.ListaImagenes);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Comprobar_Accion(this.button3, 3, this.pictureBox3, this.ListaImagenes);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Comprobar_Accion(this.button4, 4, this.pictureBox4, this.ListaImagenes);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Comprobar_Accion(this.button5, 5, this.pictureBox5, this.ListaImagenes);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Comprobar_Accion(this.button6, 6, this.pictureBox6, this.ListaImagenes);
        }
        // LA FUNCION COMPROBAR_ACCION COMPRUEBA SI HAS PULSADO EL PRIMER BOTON O EL SEGUNDO
        
        private void Comprobar_Accion(Button BotonIN, int Indice, PictureBox PictureBoxIN, ImageList ListaImagenesIN)
        {
            if (paso == 0)
            {
                // SI ES EL PRIMERO, MUESTRA LA IMAGEN, ASIGNA EL VALOR DEL ARRAY 
                // Y REPRODUCE EL SONIDO DE ABRIR
                paso = 1;
                valor1 = n[Indice].ToString();
                Boton = BotonIN;
                PictureBoxIN.Image = ListaImagenesIN.Images[n[Indice] - 1];
                Boton.Visible = false;
                AbrirSound.Play();
            }
            else if (paso == 1)
            {
                // SI ES EL SEGUNDO BOTON, INCREMENTAMOS LOS INTENTOS, MOSTRAMOS LA IMAGEN
                // Y COMPROBAMOS SI EL VAMOS DEL ARRAY ES IGUAL QUE EL DE LA PRIMERA PULSACION
                // QUE ESTA EN VALOR1
                paso = 0;
                Intentos++;
                Boton2 = BotonIN;
                Boton2.Visible = false;
                LblAciertos.Text = Intentos.ToString();
                PictureBoxIN.Image = ListaImagenesIN.Images[n[Indice] - 1];
                if (valor1 == n[Indice].ToString())
                {
                    // SI ES IGUAL, INCREMENTAMOS ACIERTOS, REPRODUCIOMOS EL SONIDO DE ACIERTO
                    // Y COMPROBAMOS SI ES EL FIN DE JUEGO
                    Aciertos++;
                    AcertarSound.Play();
                    comprobar_fin_juego();
                }

                else
                {
                    // SI NO ES IGUAL, REPRODUCIOMOS EL SONIDO DE FALLO
                    // Y ACTIVAMOS EL TIMER1, QUE LO QUE HACE ES DAR UN POCO DE ESPERA
                    // Y VUELVE A TAPAR LAS IMAGENES

                    //Boton.Visible = true;
                    //this.button6.Visible = true;
                    FallarSound.Play();
                    timer1.Enabled = true;
                }
            
            }
        }

        private void comprobar_fin_juego()
        {
           if (FinAciertos == Aciertos)
            {
                MessageBox.Show("¡Enhorabuena! ¡Eres lo mas!");
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Boton.Visible = true;
            Boton2.Visible = true;
            timer1.Enabled = false;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Comprobar_Accion(this.button7, 7, this.pictureBox7, this.ListaImagenes);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Comprobar_Accion(this.button8, 8, this.pictureBox8, this.ListaImagenes);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Comprobar_Accion(this.button9, 9, this.pictureBox9, this.ListaImagenes);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Comprobar_Accion(this.button10, 10, this.pictureBox10, this.ListaImagenes);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Comprobar_Accion(this.button11, 11, this.pictureBox11, this.ListaImagenes);
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Comprobar_Accion(this.button12, 12, this.pictureBox12, this.ListaImagenes);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            Comprobar_Accion(this.button13, 13, this.pictureBox13, this.ListaImagenes);
        }

        private void button14_Click(object sender, EventArgs e)
        {
            Comprobar_Accion(this.button14, 14, this.pictureBox14, this.ListaImagenes);
        }

        private void button15_Click(object sender, EventArgs e)
        {
            Comprobar_Accion(this.button15, 15, this.pictureBox15, this.ListaImagenes);
        }

        private void button16_Click(object sender, EventArgs e)
        {
            Comprobar_Accion(this.button16, 16, this.pictureBox16, this.ListaImagenes);
        }

        private void button17_Click(object sender, EventArgs e)
        {
            Comprobar_Accion(this.button17, 17, this.pictureBox17, this.ListaImagenes);
        }

        private void button18_Click(object sender, EventArgs e)
        {
            Comprobar_Accion(this.button18, 18, this.pictureBox18, this.ListaImagenes);
        }

        private void button19_Click(object sender, EventArgs e)
        {
            Comprobar_Accion(this.button19, 19, this.pictureBox19, this.ListaImagenes);
        }

        private void button20_Click(object sender, EventArgs e)
        {
            Comprobar_Accion(this.button20, 20, this.pictureBox20, this.ListaImagenes);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }


        //ESTA FUNCION RESETEA EL JUEGO Y LO PONE TODO PARA VOLVER A JUGAR
        private void BtnReset_Click(object sender, EventArgs e)
        {
            paso = 0;
            Aciertos = 0;
            Intentos = 0;
            MostrarBotones();
            Cargar_Imagenes();
            LblAciertos.Text = Intentos.ToString();
        }
        // LA FUNCION CARGAR IMAGENES, CARGA LAS IMAGENES ALEATORIAMENTE EN LOS CUADROS
        private void Cargar_Imagenes()
        {
            Random rand = new Random();

            //EN PRIMER LUGAR CARGO EL ARRAY RND CON VALORES ALEATORIOS
            for (int i = 1; i <= 20; i++)
            {
                Rnd[i] = rand.Next(1, 500);
            }

            // AHORA ORDENADOS EL ARRAY DE MENOR A MAYOR Y ESE MISMO ORDEN SE 
            // LO PONEMOS AL ARRAY N QUE ES EL QUE ESTAN CARGADAS LAS IMAGENES
            // BASICAMENTE LO QUE HACEMOS ES DESORDENAMOS EL ARRAY N ORDENANDO EL ARRAY RND
            // EL ORDEN DE UN ARRAY HACE EL DESORDEN DEL OTRO ARRAY
            int Menor;
            int ValorTemporal;
            for (int i = 1; i <= 20; i++)
            {
                Menor = i;
                for (int j = i + 1; j <= 20; j++)
                {
                    if (Rnd[j] < Rnd[Menor])
                    {
                        Menor = j;
                    }

                }
                //INTERCAMBIAMOS LOS NUMEROS 
                ValorTemporal = Rnd[i];
                Rnd[i] = Rnd[Menor];
                Rnd[Menor] = ValorTemporal;
                // HACEMOS EL MISMO CAMBIO CON LOS INDEX DE LAS FOTOS
                ValorTemporal = n[i];
                n[i] = n[Menor];
                n[Menor] = ValorTemporal;

            }

        }

        private void MostrarBotones()
        {
            button1.Visible = true;
            button2.Visible = true;
            button3.Visible = true;
            button4.Visible = true;
            button5.Visible = true;
            button6.Visible = true;
            button7.Visible = true;
            button8.Visible = true;
            button9.Visible = true;
            button10.Visible = true;
            button11.Visible = true;
            button12.Visible = true;
            button13.Visible = true;
            button14.Visible = true;
            button15.Visible = true;
            button16.Visible = true;
            button17.Visible = true;
            button18.Visible = true;
            button19.Visible = true;
            button20.Visible = true;


        }
    }
}





